#include "Array.hpp"
#include <iostream>
#include <string>

using namespace std;

template <class T>
void output_array(Array<T> &array){

    for(unsigned int i=0; i < array.getSize(); ++i)
        cout << array[i] << " ";

    cout << endl;
}


int main(void){
    Array<int> a(5);
    Array<string> b(5);

    for(int i=0; i < a.getSize(); ++i)
        a.Add(i+1);

    for(int i=0; i < b.getSize(); ++i){   
        b.Add("hello");
    }
    return 0;
}
