#include "SinglyLinkedList.hpp"
#include "boost/thread.hpp"

using namespace std;


template <class T>
class Queue{

    public:
        
        Queue();
 
        ~Queue();

        Queue(const Queue<T>&);

        Queue<T>& operator=(const Queue<T>&);

        T Dequeue();

        void Enqueue(const T &);
  
    private:
        SinglyLinkedList<T> *m_queue;
        boost::mutex mutex;

};

template <class T>
Queue<T>::Queue(){
    m_queue = new SinglyLinkedList<T>();
}

template <class T>
Queue<T>::~Queue(){
    m_queue->Free();
}

template <class T>
Queue<T>::Queue(const Queue<T>& other){
   
    Node<T> *curr = other.m_head;

    while(curr){
        AddLast(curr);
        curr = curr->next;
    }
}

template <class T>
Queue<T>& Queue<T>::operator=(const Queue<T> &other){

    if( this == &other) return *this;

    return *(new(this)Queue<T>(other));

}

template <class T>
T Queue<T>::Dequeue(){

    boost::mutex::scoped_lock lock(mutex);

    Node<T>* curr = m_queue->GetFirst();
    T element = curr->m_item;
    m_queue->SetFirst(curr->next);
    delete curr;
    
    return element;
 
}

template <class T>
void Queue<T>::Enqueue(const T &item){

    boost::mutex::scoped_lock lock(mutex);
 
    m_queue->AddLast(item);

}
