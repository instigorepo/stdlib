#include <iostream>
#include "SinglyLinkedList.hpp"
#include "boost/thread.hpp"

using namespace std;

const size_t DefaultSize = 10;

template <class T>
class Stack{

    public:

        Stack();

        Stack(const Stack<T>&);

        Stack<T>& operator=(const Stack<T>&);
        
        ~Stack();

        T pop();

        T top();
 
        void push(const T&);

    private:
        SinglyLinkedList<T> *m_stack;
        boost::mutex mutex; 
};

template <class T>
Stack<T>::Stack(){
    m_stack = new SinglyLinkedList<T>();
}


template <class T>
T Stack<T>::top(){
    return m_stack->GetAt(0)->m_item;
}

template <class T>
void Stack<T>::push(const T &item){
    boost::mutex::scoped_lock lock(mutex);

    m_stack->AddFirst(item);

}

template <class T>
T Stack<T>::pop(){
    boost::mutex::scoped_lock lock(mutex); 

    Node<T> *curr = m_stack->GetAt(0);
    T element = curr->m_item;
  
    m_stack->SetFirst(curr->next);
    delete curr;
    return element;
}

template <class T>
Stack<T>::Stack(const Stack<T> &other){
    Node<T> *curr = other.m_head;
 
    while(curr){
        AddFirst(curr);
        curr = curr->next;
    }
}

template <class T>
Stack<T>& Stack<T>::operator=(const Stack<T> &other){
    boost::mutex::scoped_lock lock(mutex);
    if( this == &other) return *this;

    return *(new(this)Stack<T>(other));
}

template <class T>
Stack<T>::~Stack(){
    m_stack->Free();
    m_stack = NULL;
}
