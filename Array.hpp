#include <cstring>
#include <iostream>
#include <boost/thread.hpp>

using namespace std;

const int DefaultSize = 10;

template<class T>
class Array{
    public:
        Array(int size = DefaultSize);
	
	Array(const Array &other);
	
	Array& operator=(const Array &other);

        bool Add(const T &item);
	
	T& operator[](int index){ 
            boost::mutex::scoped_lock lock(mutex);
 
            if(index >=0 && index < m_size) 
                return m_pData[index];
            cerr << "Array index is out of bounds\n";
        }
	
	int getSize() const { return m_size;}

        ~Array();
	
    private:
        boost::mutex mutex;
	T* m_pData;
	int m_capacity;
        int m_size;
};

// Name: Array
//
// Description: Constructor for the class
//
// Arguments: size of the array
//
template <class T>
Array<T>::Array(int size):m_capacity(size),m_size(0),mutex(){
    m_pData = new T[m_capacity];
}

// Name: Array
//
// Description: Copy constructor for the class
// to perform a deep copy which will not result in copy ctor / operator= errors
// 
template <class T>
Array<T>::Array(const Array& other):mutex(){
    m_size = other.getSize();
    m_capacity = other.m_capacity;
    m_pData = new T[m_capacity];
    memcpy(m_pData,other.m_pData,m_capacity);
}

// Name: Add
//
// Description: Adds an item to the Array
//
// Arguments: Item of type T to add
//
template <class T>
bool Array<T>::Add(const T &item){
    
    boost::mutex::scoped_lock lock(mutex);
    
    ++m_size;
    if( m_size > m_capacity){
        T* arrayCopy = new T[m_capacity*2];
        for(int i=0; i < m_capacity; ++i)
            arrayCopy[i] = m_pData[i];
        delete [] m_pData;
        m_pData = arrayCopy;
    }
    m_pData[m_size-1] = item;
    return true;
}

// Name: operator=
//
// Description: Copy assignment 
//
// Arguments: Other array we want to copy
//
template <class T>
Array<T>& Array<T>::operator=(const Array &other){
    
    // check for self-assignment
    if(this == &other)
        return *this;
 
    /* delete the current data and perform the copy
     * typical way of doing operator=
     * Left in for educational purposes
      
       delete [] m_pData;
       m_capacity = other.getSize();
       m_pData = new T[m_capacity];
       memcpy(m_pData,other.m_pData,m_capacity);
    */
    // let's use placement new and save memory
    // since we won't call malloc
    //
    boost::mutex::scoped_lock lock(mutex);
    this->~Array<T>(); // call the dtor on 'this'
    return *(new(this)Array<T>(other));
}

// Name: ~Array
//
// Description: Default destructor for the Array class
//              which cleans up the array items
//
template <class T>
Array<T>::~Array(){

    boost::mutex::scoped_lock lock(mutex);
    
    if(!m_pData)
        delete [] m_pData;
}

