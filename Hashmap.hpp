#include <iostream>
#include "Array.hpp"

using namespace std;


const int DefaultSize = 10;

template <class K, class V>
class Hashmap{

    public:

        Hashmap(int size = DefaultSize);

        ~Hashmap();

        int hash(char *buffer, int size);

        void put(K key, V value);

        T& get(K key);

    private:
        Array<K,V> *table; // contains tableSize elements
        int tableSize;

};

Hashmap<K,V>::Hashmap(int size):tableSize(size){
    table = new Array<K,V>[tableSize];
}

void Hashmap<K,V>::put(K key, V value){
    int idx = key.hashCode() % tableSize;
    table[idx].Add(pair<K,V>(key,value));

}

V Hashmap<K,V>::get(K key){
    Array<K,V>& a = table[hash(key,sizeof(K))];
   
    for(int i = 0; i < a.getSize(); ++i){
        pair<K,V> item = a[i];
        if( item.first == key)
            return item.second;
    }
    return V();
}


int Hashmap<K,V>::hash(char *buffer, int size){
    static const unsigned int mask = 0xdeadbeef;

    for(int i=0; i < size; ++i){
        ret ^= buf[i] ^ mask;
        ret <<= 13;
        ret >>= 9;
        ret |= buf[i] & mask;
    }
    return ret % size;

