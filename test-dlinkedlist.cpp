#include <iostream>
#include "DoublyLinkedList.hpp"

using namespace std;


// Output:
// 
// 0 1 3 5 8 
// Item at index 2: 3
// Removed the last item successfully
// 0 1 3 5  
//
int main(void){

    DoublyLinkedList<int> *list = new DoublyLinkedList<int>();
    list->Add(5);
    list->Add(1);
    list->Add(3);
    list->AddFirst(0);
    list->AddLast(8);
    list->PrintList();    

    Node<int> *t = list->GetAt(2);
    cout << "Item at index 2: " << t->m_item << endl;

    if(list->Remove(8))
        cout << "Removed the last item successfully\n";

    list->PrintList();
    return 0;
}
