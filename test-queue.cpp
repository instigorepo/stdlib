#include "Queue.hpp"

using namespace std;


int main(void){

    Queue<int> s;
    s.Enqueue(5);
    s.Enqueue(10);
   
    int topItem = s.Dequeue();
    cout << "The first item off the queue is: " << topItem << endl;

    return 0;
}
