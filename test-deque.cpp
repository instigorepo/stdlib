#include <iostream>
#include <cstdlib>
#include "Deque.hpp"


int main(void){

    try{

        Deque<int> q;
        q.AddFront(100);
        q.AddBack(200);

        cout << "Number of items in the deque: " << q.Size() << endl;
        cout << "Item at the back: " << q.Back() << endl;
        cout << "Item at the front: " << q.Front() << endl;
        cout << "Removing item at the front:" << q.Front() << endl;
        q.RemoveFront();
        cout << "Item at the front: " << q.Front() << endl;
        cout << "Item at the back: " << q.Back() << endl;
        cout << "Removing item at the back" << endl;
        q.RemoveBack();
        cout << "Item at the front: " << q.Front() << endl;
        cout << "Item at the back: " << q.Back() << endl;
    }
    catch(DequeEmptyException& x){
        // do nothing since we're testing the IsEmpty() 
        exit(-1);
    }
    return 0;
}
