#include <iostream>
#include <boost/thread.hpp>
#include <cstdlib>
#include <cstdio>

using namespace std;


// Define our Node of type T
//
template <class T>
struct Node{
    public:
	T m_item;
	Node *next;
};


template <class T>
class SinglyLinkedList{

    public:

        SinglyLinkedList();

        ~SinglyLinkedList();

        SinglyLinkedList(const SinglyLinkedList&);

        SinglyLinkedList<T>& operator=(const SinglyLinkedList&);

        Node<T>* GetAt(const size_t index);

        void AddFirst(const T&);

        void AddLast(const T&);

        void Add(const T&);

        bool Remove(const T&);

        T GetFirst() const;

        T GetLast() const;
 
        bool IsEmpty();

        void PrintList();

        size_t Count() const;
        
        Node<T>* GetFirst();

        void SetFirst(Node<T>*);

        void SetLast(Node<T>*);

        void Free();

    private:
        Node<T> *m_head,*m_tail;
        size_t m_size;
        boost::mutex mutex;

};

// Name: Count
// 
// Description: Determines the number of items in the list
//
// Returns: Size value of the list
template <class T>
size_t SinglyLinkedList<T>::Count() const{
    boost::mutex::scoped_lock lock(mutex); 
    return m_size; 
}

// 
// Name: SetFirst
//
// Description: Sets the head node in the list
//
template <class T>
void SinglyLinkedList<T>::SetFirst(Node<T> *item){
    
    if(m_head)
        m_head = item;
}


// 
// Name: SetLast
//
// Description: Sets the given node item to be the last item
// in the list. This is to be used in special cases (i.e. accessing m_tail ) 
//
//
template <class T>
void SinglyLinkedList<T>::SetLast(Node<T> *item){

    if(m_tail && !item->next) 
        m_tail = item;
    else
        cout << "Cannot set node to be last node\n";
}

//
// Name: GetFirst
//
// Description: Retrieve the first item in the list
//
// Return: The first node in the list
//
template <class T>
Node<T>* SinglyLinkedList<T>::GetFirst(){
    boost::mutex::scoped_lock lock(mutex); 
    return m_head;
}

// Name: SinglyLinkedList
//
// Description: Default constructor for the class
//
template <class T>
SinglyLinkedList<T>::SinglyLinkedList(): m_head(NULL),m_tail(NULL),m_size(0){
}

// Name: operator=
//
// Description: Assignment operator for the class
//
template <class T>
SinglyLinkedList<T>& SinglyLinkedList<T>::operator=(const SinglyLinkedList& other){
    if( this != &other){
        return new(this)SinglyLinkedList<T>(other);
    }
    return *(new(this)SinglyLinkedList<T>(other));
}

// Name: SinglyLinkedList
//
// Description: Copy constructor for the class
//
template <class T>
SinglyLinkedList<T>::SinglyLinkedList(const SinglyLinkedList& other):m_head(NULL),m_tail(NULL),m_size(0)
{
    Node<T> *curr = other.m_head;
   
    while(curr){
        Add(curr->m_item);
        curr = curr->next;
    } 
}

// 
// Name: IsEmpty
//
// Description: Determines if the list is empty
//
// Returns: True if the list is empty, else false
//
template <class T>
bool SinglyLinkedList<T>::IsEmpty(){
    boost::mutex::scoped_lock lock(mutex);
    return m_size == 0;
}

// 
// Name: ~SinglyLinkedList
//
// Description: Destructor for the class
//
template <class T> 
SinglyLinkedList<T>::~SinglyLinkedList(){
    Free();
    delete m_head;
}

// 
// Name: GetAt
//
// Description: Get an item at the given index location
//
// Argument(s): Given index
//
// Return: Node item at the given index
//
template <class T>
Node<T>* SinglyLinkedList<T>::GetAt(const size_t index){

    boost::mutex::scoped_lock lock(mutex);
    Node<T>* tmp = m_head;
    
    if(index < 0 || index > m_size){
        cout << "Given index is out of bounds\n";
        return tmp;
    }
    int currIdx = 0;
    while(tmp){
        if(currIdx++ == index){
            return tmp;
        }
        tmp = tmp->next;
    }
    return tmp;
}

//
// Name: AddFirst
// 
// Description: Adds an item to the beginning of the list
//
// Argument(s): Item to add
//
template <class T>
void SinglyLinkedList<T>::AddFirst(const T &item){
    
    boost::mutex::scoped_lock lock(mutex);
    Node<T> *curr = m_head;
    Node<T> *newNode = new Node<T>();

    if(newNode){
        newNode->m_item = item;
        newNode->next = NULL;
    }
    else{
        perror("No memory exists\n");
        exit(EXIT_FAILURE);
    }

    // case 1: empty list
    if(!curr){
        m_head = newNode;
        m_size++;
        return;
    }
 
    // case 2: at least 1 item
    newNode->next = m_head;
    m_head = newNode;
    m_size++;
}

//
// Name: GetFirst
//
// Description: Retrieves the first item in the list
//
// Return: The first item's value
//
template <class T>
T SinglyLinkedList<T>::GetFirst() const{
    return m_head->m_item;
}


// 
// Name: GetLast
//
// Description: Retrieves the last item in the list
//
// Return: The last item's value
template <class T>
T SinglyLinkedList<T>::GetLast() const{
    return m_tail->m_item;
}

//
// Name: AddLast
//
// Description: Adds an item to the end of the list
//
// Argument(s): Item to add
//
template <class T>
void SinglyLinkedList<T>::AddLast(const T &item){

    boost::mutex::scoped_lock lock(mutex);
    Node<T> *curr = m_tail;
    Node<T> *newNode = new Node<T>();

    if(newNode){
        newNode->m_item = item;
        newNode->next = NULL;
    }
    else{
        perror("No memory exists\n");
        exit(EXIT_FAILURE);
    }

    // case 1: empty list
    if(!curr){
        m_tail = m_head = newNode;
        m_size++;
        return;
    } 

    while(curr && curr->next){
        curr = curr->next;
    }
    curr->next = newNode;
    m_tail = curr;
}

//
// Name: Add
//
// Description: Adds an item to the list
//
// Argument(s): Item to add
//
template <class T>
void SinglyLinkedList<T>::Add(const T &item){

    boost::mutex::scoped_lock lock(mutex);
    Node<T> *curr = m_head;
    Node<T> *prev = NULL;
    Node<T> *newNode = new Node<T>();

    if(newNode){
        newNode->m_item = item;
        newNode->next = NULL;
    }
    else{
        perror("No memory exists\n");
        exit(EXIT_FAILURE);
    }

    // 1. case of an empty list 
    if(!curr){
        m_tail = m_head = newNode;
        ++m_size;
        return;
    }
  
    // 2. case for one item in the list
    while(curr){
        if(curr->m_item > item){
            break;
        }
        else{
            prev = curr;
            curr = curr->next;
        }
    }
    if(prev){
        prev->next = newNode;
    }
    else{
        m_tail = m_head = newNode;
    }
    newNode->next = curr;
    ++m_size;
}

// 
// Name: Remove
//
// Description: Removes an item from the list
//
// Argument(s): Item to be removed
//
template <class T>
bool SinglyLinkedList<T>::Remove(const T &item){
   
    boost::mutex::scoped_lock lock(mutex);
    Node<T> *tmp = m_head;
    Node<T> *prev = NULL;

    if(!tmp){ 
        cout << "The list is empty\n"; 
        return false; 
    }

    // visit each item in the list
    while(tmp){
        if(tmp->m_item == item)
            break;
        prev = tmp;
        tmp = tmp->next;
    }
    // remove from the beginning
    if(!prev){
        m_head = tmp->next;
    }
    else if(!tmp->next){
        // end case
        prev->next = tmp->next;
        m_tail = prev;
    } 
    else{
        // middle case
        prev->next = tmp->next;
    }
    // dealloc the node
    delete tmp;

    // we are done searching
    return true; 
}

// Name: PrintList
//
// Description: Print the contents of the list
//
template <class T>
void SinglyLinkedList<T>::PrintList(){
    
    boost::mutex::scoped_lock lock(mutex);

    Node<T> *tmp = m_head;

    if(!tmp){
        cout << "The list is empty\n";
        return;
    }

    while(tmp){
        cout << tmp->m_item << " ";
        tmp = tmp->next;
    }
    cout << endl;
}

// Name: Free
//
// Description:  Deallocate the allocated space for the list
//
template <class T>
void SinglyLinkedList<T>::Free(){
    boost::mutex::scoped_lock lock(mutex);
    Node<T> *curr = NULL;
    Node<T> *tmp = m_head;

    while(tmp){
        curr = tmp;
        tmp = tmp->next;
        delete curr;
    }   
    m_tail = m_head = NULL;
    m_size = 0; 
}
