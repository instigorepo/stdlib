#include "Stack.hpp"

using namespace std;


int main(void){

    Stack<int> s;
    s.push(5);
    s.push(10);
   
    int topItem = s.top();
    cout << "The top item is: " << topItem << endl;
    int popItem = s.pop();
    cout << "The item pop()'d is: " << popItem << endl;
    int newTop = s.top();
    cout << "The new top() is: " << newTop << endl;

    return 0;
}
