#include <iostream>
#include <boost/thread.hpp>

using namespace std;

//
// Define our Node of type T
//
template <class T>
struct Node{

        T m_item;
        Node *next, *prev;
};

template <class T>
class DoublyLinkedList{

    public:

        DoublyLinkedList();

        ~DoublyLinkedList();

        DoublyLinkedList(const DoublyLinkedList&);

        DoublyLinkedList<T>& operator=(const DoublyLinkedList&);

        Node<T>* GetAt(const size_t index);

        void AddFirst(const T&);

        void AddLast(const T&);

        void Add(const T&);

        bool Remove(const T&);
 
        bool IsEmpty();

        void PrintList();

        void Free();

        // Properties
        int Count() const { return m_size; }
        
        Node<T>* First() const { 

            return m_head; 
        }

    private:
        Node<T> *m_head;
        int m_size;

};

//
// Name: DoublyLinkedList
//
// Description: Default constructor for the class
//
template <class T>
DoublyLinkedList<T>::DoublyLinkedList(): m_head(NULL),m_size(0){
}

//
// Name: operator=
//
// Description: Assignment operator for the class
//
// Argument(s): Other list to copy
//
template <class T>
DoublyLinkedList<T>& DoublyLinkedList<T>::operator=(const DoublyLinkedList& other){
    if( this != &other){
        return new(this)DoublyLinkedList<T>(other);
    }
    return *(new(this)DoublyLinkedList<T>(other));
}


//
// Name: DoublyLinkedList - Copy Ctor
//
// Description: Copy Constructor for the class
//
// Argument(s): Other list to copy
//
template <class T>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList& other):m_head(NULL),m_size(0)
{

    Node<T> *curr = other.m_head;
   
    while(curr){
        Add(curr->Item());
        curr = curr->Next();
    } 
}

//
// Name: IsEmpty
//
// Description: Determines if the list is empty
//
// Returns: True if the list is empty, else false
//
template <class T>
bool DoublyLinkedList<T>::IsEmpty(){
    
    return m_size == 0;
}

//
// Name: ~DoublyLinkedList
//
// Description: Destructor for the class
//
template <class T> 
DoublyLinkedList<T>::~DoublyLinkedList(){
    Free();
}

// 
// Name: GetAt
//
// Description: Get an item at the given index location
//
// Argument(s): Given index
//
template <class T>
Node<T>* DoublyLinkedList<T>::GetAt(const size_t index){

    Node<T>* tmp = m_head;
    
    if(index < 0 || index > m_size){
        cout << "Given index is out of bounds\n";
        return tmp;
    }
    int currIdx = 0;
    while(tmp){
        if(currIdx == index){
            return tmp;
        }
        tmp = tmp->next;
        ++currIdx;
    }
    return tmp;

}


//
// Name: AddFirst
// 
// Description: Adds an item to the beginning of the list
//
// Argument(s): Item to add
//
template <class T>
void DoublyLinkedList<T>::AddFirst(const T &item){

    Node<T> *newNode = new Node<T>();
    Node<T> *curr = m_head;

    if(newNode){
        newNode->m_item = item;
        newNode->prev = newNode->next = NULL;
    }

    // case 1: empty list
    if(!curr){
        m_head = newNode;
        m_size++;
        return;
    }
 
    // case 2: at least 1 item
    newNode->next = m_head;
    curr->prev = newNode;
    m_head = newNode;
    m_size++;
}

//
// Name: AddLast
//
// Description: Adds an item to the end of the list
//
// Argument(s): Item to add
//
template <class T>
void DoublyLinkedList<T>::AddLast(const T &item){

    Node<T> *newNode = new Node<T>();
    Node<T> *curr = m_head;

    if(newNode){
        newNode->m_item = item;
        newNode->prev = newNode->next = NULL;
    }

    // case 1: empty list
    if(!curr){
        m_head = newNode;
        m_size++;
        return;
    } 

    while(curr && curr->next){
        curr = curr->next;
    }

    curr->next = newNode;
    newNode->prev = curr;
}

//
// Name: Add
//
// Description: Adds an item to the list
//
// Argument(s): Item to add
//
template <class T>
void DoublyLinkedList<T>::Add(const T &item){

    Node<T> *curr = m_head;
    Node<T> *prev = NULL;
    Node<T> *newNode = new Node<T>();

    if(newNode){
        newNode->m_item = item;
        newNode->prev = newNode->next = NULL;
    }

    // 1. case of an empty list 
    if(!curr){
        m_head = newNode;
        ++m_size;
        return;
    }
  
    // 2. case for one item in the list
    while(curr){
        if(curr->m_item > item){
            break;
        }
        else{
            prev = curr;
            curr = curr->next;
        }
    }
    if(prev){
        prev->next = newNode;
        newNode->prev = prev;
    }
    else{
        m_head = newNode;
    }
    if(curr){
        curr->prev = newNode;
    }
    newNode->next = curr;
    ++m_size;
}


// 
// Name: Remove
//
// Description: Removes an item from the list
//
// Argument(s): Item to be removed
//
template <class T>
bool DoublyLinkedList<T>::Remove(const T &item){
   
    Node<T> *tmp = m_head;
    Node<T> *prev = NULL;

    if(!tmp){ 
        cout << "The list is empty\n"; 
        return false; 
    }

    // visit each item in the list
    while(tmp){
        if(tmp->m_item == item)
            break;
        prev = tmp;
        tmp = tmp->next;
    }
    // remove from the beginning
    if(!prev){
        m_head = tmp->next;
    }
    else if(!tmp->next){
        // end case
        prev->next = tmp->next;
    } 
    else{
        // middle case
        prev->next = tmp->next;
        tmp->next->prev = prev;
    }
    // dealloc the node
    delete tmp;

    // we are done searching
    return true; 
}

// Name: PrintList
//
// Description: Print the contents of the list
//
template <class T>
void DoublyLinkedList<T>::PrintList(){
    Node<T> *tmp = m_head;

    if(!tmp){
        cout << "The list is empty\n";
        return;
    }

    while(tmp){
        cout << tmp->m_item << " ";
        tmp = tmp->next;
    }
    cout << endl;
}

// Name: Free
//
// Description:  Deallocate the allocated space for the list
//
template <class T>
void DoublyLinkedList<T>::Free(){
    Node<T> *curr = NULL;
    Node<T> *tmp = m_head;

    while(tmp){
        curr = tmp;
        tmp = tmp->next;
        delete curr;
    }   
    delete m_head;
    m_size = 0; 
}
