#include <iostream>
#include "SinglyLinkedList.hpp"

using namespace std;


// Output:
// 
// 0 1 3 5 8 
// Item at index 2: 3
// Removed the last item successfully
// 0 1 3 5  
//
int main(void){

    SinglyLinkedList<int> *list = new SinglyLinkedList<int>();
    list->Add(5);
    list->Add(1);
    list->Add(3);
    list->AddFirst(0);
    list->AddLast(8);
    list->Add(5);
    list->PrintList();    

    Node<int> *t = list->GetAt(2);
    cout << "Item at index 2: " << t->m_item << endl;

    list->SetLast(t);
    if(list->Remove(8))
        cout << "Removed the last item successfully\n";

    int item = list->GetFirst()->m_item;
    cout << "FirstItem: " << item << endl;

    cout << "List after Remove(): ";
    list->PrintList();

    SinglyLinkedList<int> l2(*list);    
    cout << "List after copy ctor is called: ";
    l2.PrintList();

    SinglyLinkedList<int> l3 = l2;
    cout << "List after operator=() called: ", l3.PrintList();

       
 
    return 0;
}
